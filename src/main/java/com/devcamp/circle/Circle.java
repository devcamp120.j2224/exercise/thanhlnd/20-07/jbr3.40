package com.devcamp.circle;

public class Circle {
    private double radius = 1.0;

    Circle(){};
    Circle(double radius){
        this.radius = radius;
    };

    public double getRadius(){
        return this.radius;
    }

    public void setRadius(double radius){
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI*radius*radius;
    }

    public double getCircumference(){
        return Math.PI*2*radius;
    }

    @Override
    public String toString(){
        return String.format("Circle[radius= %s]", radius);
    }
}
