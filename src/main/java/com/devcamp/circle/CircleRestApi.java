package com.devcamp.circle;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleRestApi {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double circleArea(@RequestParam(value="radius")double radius){
        Circle circle = new Circle(radius);
        return circle.getArea();
    }
}
